// Example of a FACTORY PATTERN

(function(){


    function makeGreeting(langauge){

        // 'language' variable will be fetched through the SCOPE CHAIN
        if (language == 'en') {
            console.log('Hello' + firstname + ' ' + lastname);
        }

        if (language == 'es') {
            console.log('Hola el bebe' + firstname + ' ' + lastname);
        }
    }

    // Becouase of CLOSURES the two functions below will have a different EXECUTION CONTEXT
    
    // LEXICAL ENVIRONMENT will have the 'language' parameter set to 'en'
    var greetEnglish = makeGreeting('en')

    //LEXICAL ENVIRONMENT will have the 'language' parameter set to 'es'
    var greetSpanish = makeGreeting('es')


})()
