(function(){


    log = console.log

    // VARIABLES ARE PASSED BY VALUE

    let a = 10
    let b = 'Hi'

    // 'a' passed its VALUE to 'c'
    let c = a
    c = c + 1
    log(a) // 10 because its 'a' defferent reference
    log(c) // 11 because its 'a' different reference than 'c'

    
    // ARRAYS AND OBJECT ARE PASSED BY REFERENCE

    let d = [1, 2]
    let e = d
    e[0] = 3
    log(d[0]) // 3 because 'd' and 'e' are are the same Array and thus have same REFERENCE

    // Now i give 'e' a REFERENCE of its own
    e = [4, 5, 6] // 'e' has a new ARRAY LITERAL
    log(d[0]) // still 3
    log(e[0]) // 4 as 'e' is new ARRAY with new REFERENCE

    let f = [4, 5, 6]
    log(e == f) // this is 'false' as 'f' is not same REFERENCE as 'e'

    f = e
    log(e == f) // this is 'true' as 'e' passed its REFERENCE to 'f' essantialy being the same value

    function modifyArray(array, element, change){
        // array parameter has REFERENCE of whatever element is set as argument
        array[element] = change
    }

    modifyArray(e, 0, 13)
    log(e[0]) // 'e' is modified because it wass passed by REFERENCE to the argument


})()
