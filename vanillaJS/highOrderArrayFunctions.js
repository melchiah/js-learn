(function(){


    const companies = [
        {name: "Company One", category: "Finance", start: 1991, end: 2003},
        {name: "Company Two", category: "Retail", start: 1992, end: 2003},
        {name: "Company Three", category: "Auto", start: 1999, end: 2011},
        {name: "Company Four", category: "Retail", start: 1989, end: 2009},
        {name: "Company Five", category: "Technology", start: 2009, end: 2017},
        {name: "Company Six", category: "Finance", start: 2002, end: 2010},
        {name: "Company Seven", category: "Auto", start: 1986, end: 1999},
        {name: "Company Eight", category: "Technology", start: 2001, end: 2015},
        {name: "Company Nine", category: "Retail", start: 1981, end: 1989},
    ]

    const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];


    const celsius = [22, 27, 35, 38, 40, 8, 1, 0]

    //forEach
    //Simply loop through array using Array Object
    companies.forEach(function(company){
        //console.log(company.name)
    })

    //filter
    //Returns an Array wich are chosen by bool return from callback iterating through each element
    const canDrinkAlchohol = ages.filter(function(age){
        if(age >= 21){
            return true
        }
    })
    //console.log(canDrinkAlchohol)

    const fromFinance = companies.filter(function(person){
        return person.category === "Finance"
    })
    //console.log(fromFinance)

    const lastedTenYears = companies.filter(company => (company.end - company.start >= 10))
    //console.log(lastedTenYears)

    //map
    // Reformat the array used to your preference.
    // Map is used TO MANIPULATE YOUR DATA
    const testMap = companies.map(function(company){
        return `${company.name} [${company.start} - ${company.end}]`
    })
    //Make map more neat
    const otherTestMap = companies.map(company => `${company.name} [${company.start} - ${company.end}]`)
    //console.log(otherTestMap)

    const fharenheit = celsius
        .map(temp => temp * 9/5)
        .map(temp => temp + 32)
    //console.log(celsius)
    //console.log(fharenheit)

    //sort
    // Sort and ORDER BY arrays or FROM propertes of arrays.
    /**
    const sortedAges = ages.sort(function(a, b){
        if (b > a) {
           return 1 
        } else {
            return -1
        }
    })
    console.log(sortedAges)
     */

    const sortedAges = ages.sort((a, b) => (b > a) ? 1 : -1)
    console.log(sortedAges)

    //reduce
    // Does a SUMMATION of all elements or element.properties in the array
    const totalYears = companies.reduce((total, company) => total + (company.end - company.start), 0)
    console.log(totalYears)


})()
