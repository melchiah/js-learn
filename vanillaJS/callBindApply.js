(function(){

    var person = {
    
        firstname: 'John',
        lastname: 'Doe',
        getFullName: function(){

            return this.firstname + ' ' + this.lastname

        }

    }

    var logName = function(lang1, lang2){

        console.log('Logged: ' + this.getFullName())
        console.log('Arguments: ' + lang1 + ' ' + lang2)
        console.log('------------')

    }

    // You can use the call(), bind(), apply() methods to change the CONTEXT of the this KEYWORD
    // and point 'this' to whichever object instance you like.

    //  bind() method will return a function. Now in the CONTEXT of 'person' for 'this' prototype inheritance
    var logPersonName = logName.bind(person)
    logPersonName('en', 'es')

    //  call() method will instead execute instantly with its new CONTEXT and also can add additional parameters
    logName.call(person, 'fr', 'gb')

    //  apply() method bahaves like call() method, but parameters are to be inserted in an ARRAY
    logName.apply(person, ['en', 'mt'])

})()
