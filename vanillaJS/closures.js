//Closures
//How inner anonymous function has access to the whattosay variable?

(function(){


    function greet(whattosay){

        return function(name) {
        
            console.log(whattosay + ' ' + name)
        }

    }

    greet('Hi')('Tony')

    function outerFunction(){

        var outer_var = "From Outer Varaible"

        function innerFunction(){

            function innerInnerFunc(){

                console.log(outer_var)
                // Inner functions will have access to the outer SCOPE CHAIN

            }

            innerInnerFunc();
        }

        innerFunction();
    }

    outerFunction();


})()

