(function(){

    const dragons = [
        { name: 'fluffykins', element: 'lightning'},
        { name: 'noomi', element: 'lightning'},
        { name: 'karo', element: 'fire'},
        { name: 'doomer', element: 'timewarp'},
    ]

    const hasElement = element => object => object.element === element

    const lightningDragons = dragons.filter(hasElement('lightning'))

    //console.log(lightningDragons)


    //let dragon = name => size => element => name + ' is a ' + size + ' dragon that breathes ' + element + '!'

    let dragon = function(name, size, element){
        return name + ' is a ' + size + ' dragon that breathes ' + element + '!'
    }

    console.log(dragon('blaago', 'large', 'frost'))

    let dragoncurry = (name) => (size) => (element) => {
        return name + ' is a ' + size + ' dragon that breathes ' + element + '!' 
    }

    let dragonName = dragoncurry('Fenlo')
    let dragonSize = dragonName('small')
    let dragonElement = dragonSize('posion')
    console.log(dragonElement)

})()
